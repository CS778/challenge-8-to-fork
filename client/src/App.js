import React, { Component } from "react";
import "./App.css";
import Footer from "./components/partials/Footer.js";
import Register from "./components/Register";
import Home from "./components/Home";
import Edit from "./components/Edit";
import Search from "./components/Search";

class App extends Component {
  state = {
    show: "",
  };

  handleHome = () => {
    this.setState({ show: "Home" });
  };
  handleRegister = () => {
    this.setState({ show: "Register" });
  };
  handleEdit = () => {
    this.setState({ show: "Edit" });
  };
  handleSearch = () => {
    this.setState({ show: "Search" });
  };

  render() {
    return (
      //! REACT Default
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <button className="btn btn-warning">Start React</button>
      //   </header>
      // </div>
      //!
      <div className="App">
        <nav class="navbar navbar-expand-lg bg-warning">
          <div class="container-fluid">
            <span class="navbar-brand">
              <b>THE GAME !</b>
            </span>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div class="navbar-nav">
                <span class="nav-link" onClick={this.handleHome}>
                  Home
                </span>
                <span class="nav-link" onClick={this.handleRegister}>
                  Register
                </span>
                <span class="nav-link" onClick={this.handleEdit}>
                  Edit
                </span>
                <span class="nav-link" onClick={this.handleSearch}>
                  Search
                </span>
              </div>
            </div>
          </div>
        </nav>
        <div>
          {this.state.show === "" && <Home />}
          {this.state.show === "Home" && <Home />}
          {this.state.show === "Register" && <Register />}
          {this.state.show === "Edit" && <Edit />}
          {this.state.show === "Search" && <Search />}
        </div>

        <Footer />
      </div>
    );
  }
}

export default App;
