import { React, Component } from "react";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "" };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  handleSubmit(event) {
    alert("A name was submitted: " + this.state.email + this.state.password);
    event.preventDefault();
  }

  render() {
    return (
      //   <form onSubmit={this.handleSubmit}>
      //     <label>
      //       Email:
      //       <input
      //         type="email"
      //         value={this.state.email}
      //         onChange={this.handleEmailChange}
      //       />
      //     </label>
      //     <br />
      //     <label>
      //       Password:
      //       <input
      //         type="password"
      //         value={this.state.password}
      //         onChange={this.handlePasswordChange}
      //       />
      //     </label>
      //     <br />

      //     <input type="submit" value="Submit" />
      //   </form>
      <form onSubmit={this.handleSubmit}>
        <div class="card-body p-md-5 text-black">
          <div class="row">
            <div class="col-md-6 mb-4">
              <div class="form-outline">
                <input
                  type="email"
                  value={this.state.email}
                  onChange={this.handleEmailChange}
                  class="form-control form-control-lg"
                />
                <label class="form-label" for="form3Example1m">
                  Email
                </label>
              </div>
            </div>
            <div class="col-md-6 mb-4">
              <div class="form-outline">
                <input
                  type="password"
                  value={this.state.password}
                  onChange={this.handlePasswordChange}
                  class="form-control form-control-lg"
                />
                <label class="form-label">Password</label>
              </div>
            </div>
          </div>

          <div class="d-flex justify-content-end pt-2">
            <button type="submit" class="btn btn-warning btn-lg ms-2">
              Submit form
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default Register;
