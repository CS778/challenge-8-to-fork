import { React, Component } from "react";
import LogoBinar from "../assets/img/new_binar_logo.jpeg";

const imgStyle = {
  margin: "10px",
  float: "left",
  height: "50px",
  width: "50px",
};

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = { value: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    alert("A name was submitted: " + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <div className="edit_container">
        <div className="logoBinar" style={imgStyle}>
          <img src={LogoBinar} style={imgStyle} alt="Logo" />
        </div>
        <div className="edit">SEARCH</div>
      </div>
    );
  }
}

export default Edit;
